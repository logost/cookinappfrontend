import { USER_LOGGED_IN, USER_LOGGED_OUT } from "../types";
import api from "../api";
import setAuthorizationHeader from "../utils/setAuthorizationHeader";

export const userLoggedIn = (user) => ({
  type: USER_LOGGED_IN,
  user,
});

export const userLoggedOut = (user) => ({
  type: USER_LOGGED_OUT,
  user,
});

export const login = (credentials) => (dispatch) =>
  api.user.login(credentials).then((user) => {
    window.location.reload(true);
    localStorage.TOKEN_USER_AUTH = user.token;
    setAuthorizationHeader(user);
    dispatch(userLoggedIn(user));
  });

export const logout = () => (dispatch) => {
  localStorage.removeItem("TOKEN_USER_AUTH");
  setAuthorizationHeader();
  dispatch(userLoggedOut());
};

export const deleteAccount = (credentials) => (dispatch) =>
  api.user.deleteAccount(credentials).then((user) => {
    localStorage.removeItem("TOKEN_USER_AUTH");
    setAuthorizationHeader();
    dispatch(userLoggedOut());
  });

export const confirm = (token) => (dispatch) =>
  api.user.confirm(token).then((user) => {
    localStorage.TOKEN_USER_AUTH = user.token;
    dispatch(userLoggedIn(user));
  });

export const resetPasswordRequest = ({ email }) => () =>
  api.user.resetPasswordRequest(email);

export const validateToken = (token) => () => api.user.validateToken(token);

export const resetPassword = (data) => () => api.user.resetPassword(data);
