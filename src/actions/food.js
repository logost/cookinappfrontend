import api from "../api";

export const getFoodName = (foodName) => (dispatch) =>
  api.food.getFoodName(foodName);

export const getFoodInfo = (foodId) => (dispatch) =>
  api.food.getFoodInfo(foodId);

export const getFoodUnit = (foodId) => (dispatch) =>
  api.food.getFoodUnit(foodId);
