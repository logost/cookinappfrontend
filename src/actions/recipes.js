import { RECIPE_CREATE, RECIPE_DELETE } from "../types";
import api from "../api";

export const createNewRecipe = (recipe) => ({
  type: RECIPE_CREATE,
  recipe,
});

export const recipeRemove = () => ({
  type: RECIPE_DELETE,
});

export const createRecipe = (recipe) => (dispatch) => {
  return api.recipe.createRecipe({
    title: recipe.title,
    description: "string",
    instructions: recipe.instructions.map((item) => item.todo),
    ingredients: recipe.ingredients.map(
      (item) =>
        (item = {
          food_id: item.id,
          quantity: parseFloat(item.quantity),
          unit_id: item.unit,
        })
    ),
    servings: recipe.serving,
    preparation_time_minutes: recipe.cookingTime,
    base_64_encoded_picture: recipe.base64,
  });
};

export const getRecipeid = (recipe_id, jwt) => (dispatch) => {
  return api.recipe.getRecipeid(recipe_id, jwt);
};

export const getRecipe = (recipeTitle, pseudo, arrayFood_id) => (dispatch) => {
  var findRecipe;
  console.log(recipeTitle);
  if (recipeTitle != null) {
    findRecipe = "&title=" + recipeTitle;
  }
  if (pseudo != null) {
    findRecipe = findRecipe + "&pseudo=" + pseudo;
  }
  if (arrayFood_id.length !== 0) {
    findRecipe = findRecipe + "&ingredients=[" + arrayFood_id + "]";
  }
  return api.recipe.getRecipe(findRecipe);
};
