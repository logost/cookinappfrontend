import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

class MyAccountPage extends React.Component {

  render() {
    return (
      <div>
        <h1>My Account</h1>
        <Button primary as={Link} to="/modifyAccount">Change my account</Button>
<br/><br/><br/>
        <Button primary as={Link} to="/deleteAccount">Delete my account</Button>
      </div>
    );
  }
}

MyAccountPage.propTypes = {
  isConfirmed: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isConfirmed: !!state.user.confirmed
  };
}

export default connect(mapStateToProps)(MyAccountPage);
