import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import imagesTest from "../../images/placeholder-profile.png";
import { Link } from "react-router-dom";
import { Grid, List, Form } from "semantic-ui-react";
import imagesTestRecipe from "./test.jpg";
import { getRecipe } from "../../actions/recipes";
import { getMyFavourite } from "../../actions/favourite";

class ProfilePage extends React.Component {
  state = {
    myRecipes: [],
    favouriteRecipes: [],
  };

  onRecipeSearch = (recipe) => {
    console.log(recipe);
  };

  componentDidMount = () => {
    this.props.getRecipe("", "plop", "").then((myRecipes) => {
      console.log(myRecipes.map((recipe) => recipe.is_favourite));
      return this.setState({ myRecipes: myRecipes });
    });
    this.props.getMyFavourite().then((favouriteRecipes) => {
      console.log(favouriteRecipes.map((recipe) => recipe));
      return this.setState({ favouriteRecipes: favouriteRecipes });
    });
  };

  render() {
    const { myRecipes, favouriteRecipes } = this.state;
    return (
      <div>
        <img src={imagesTest} style={{ width: "200px" }} alt="description" />

        <List divided>
          <h3>Liste des recettes que j'ai cree:</h3>
          {myRecipes.map((item) => {
            var recipeId = item.recipe_id.toString();
            return (
              <List.Item
                key={item.recipe_id}
                style={{ paddingBottom: "20px", paddingTop: "20px" }}
              >
                <Form.Field>
                  <Grid>
                    <Grid.Column
                      width={3}
                      as={Link}
                      to={`/recipe/${recipeId.toString()}`}
                      header
                    >
                      <img
                        src={imagesTestRecipe}
                        style={{ width: "200px" }}
                        alt="description"
                      />
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <h2>
                        <a href={"/recipe/" + item.recipe_id}>{item.title}</a>
                      </h2>
                      {item.instructions.map((instruction) => {
                        var unique_id = item.recipe_id + instruction;
                        return <div key={unique_id}>{instruction}</div>;
                      })}
                    </Grid.Column>
                  </Grid>
                </Form.Field>
              </List.Item>
            );
          })}
        </List>
        <List divided>
          <h3>Liste de mes recettes favorites:</h3>
          {favouriteRecipes.map((item) => {
            var recipeId = item.recipe_id.toString();
            return (
              <List.Item
                key={item.recipe_id}
                style={{ paddingBottom: "20px", paddingTop: "20px" }}
              >
                <Form.Field>
                  <Grid>
                    <Grid.Column
                      width={3}
                      as={Link}
                      to={`/recipe/${recipeId.toString()}`}
                      header
                    >
                      <img
                        src={imagesTestRecipe}
                        style={{ width: "200px" }}
                        alt="description"
                      />
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <h2>
                        <a href={"/recipe/" + item.recipe_id}>{item.title}</a>
                      </h2>
                      {item.instructions.map((instruction) => {
                        var unique_id = item.recipe_id + instruction;
                        return <div key={unique_id}>{instruction}</div>;
                      })}
                    </Grid.Column>
                  </Grid>
                </Form.Field>
              </List.Item>
            );
          })}
        </List>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  getRecipe: PropTypes.func.isRequired,
  getMyFavourite: PropTypes.func.isRequired,
};

export default connect(null, { getRecipe, getMyFavourite })(ProfilePage);
