import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import CreateRecipeForm from "../../forms/CreateRecipeForm";
import { createRecipe } from "../../../actions/recipes";

class CreateRecipePage extends React.Component {
  submit = (data) =>
    this.props
      .createRecipe(data)
      .then(() => this.props.history.push("/dashboard"));

  render() {
    return (
      <div>
        <h1>Create new recipe</h1>
        <CreateRecipeForm submit={this.submit} />
      </div>
    );
  }
}

CreateRecipePage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  createRecipe: PropTypes.func.isRequired,
};

export default connect(null, { createRecipe })(CreateRecipePage);
