import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form, Dropdown } from "semantic-ui-react";
import { getFoodName } from "../../actions/food";

class SearchIngredientForm extends React.Component {
  state = {
    query: "",
    loading: false,
    options: [],
    reqIngredients: {},
  };

  onSearchChange = (e, data) => {
    clearTimeout(this.timer);
    this.setState({
      query: data,
    });
    this.timer = setTimeout(this.fetchOptions, 500);
  };

  onChange = (e, data) => {
    console.log(this.state.reqIngredients[data.value]);
    this.setState({ query: data.value });
    this.props.onIngredientSelect(this.state.reqIngredients[data.value]);
  };

  fetchOptions = () => {
    if (!this.state.query) return;
    this.setState({ loading: true });
    this.props
      .getFoodName(this.state.query.searchQuery)
      .then((foods) => {
        return foods;
      })
      .then((ingredients) => {
        const options = [];
        const ingredientsHash = {};
        ingredients.forEach((ingredients) => {
          ingredientsHash[ingredients.name] = ingredients;
          options.push({
            key: ingredients.unit_id,
            value: ingredients.name,
            text: ingredients.name,
          });
        });
        this.setState({
          loading: false,
          options,
          reqIngredients: ingredientsHash,
        });
      });
  };

  render() {
    return (
      <Form.Field>
        <Dropdown
          search
          fluid
          placeholder="Rechercher un ingredient par nom"
          value={this.state.query.searchQuery}
          onSearchChange={this.onSearchChange}
          options={this.state.options}
          loading={this.state.loading}
          onChange={this.onChange}
        />
      </Form.Field>
    );
  }
}

SearchIngredientForm.propTypes = {
  onIngredientSelect: PropTypes.func.isRequired,
  getFoodName: PropTypes.func.isRequired,
};

export default connect(null, { getFoodName })(SearchIngredientForm);
