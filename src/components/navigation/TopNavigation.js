import React from "react";
import PropTypes from "prop-types";
import { Container, Dropdown, Image, Menu, Icon } from "semantic-ui-react";
import {
  Nav,
  Navbar,
  NavDropdown,
  Form,
  FormControl,
  Button,
} from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import * as actions from "../../actions/auth";
import logo from "./CookingAppIcon.png";
import SearchRecipeForm from "../forms/SearchRecipeForm";
import "./styleNavbar.css";

const trigger = (
  <span>
    <Icon name="user" /> Hello, Bob
  </span>
);

const TopNavigation = ({ isAuthenticated, user, logout }) => (
  <Navbar fixed="top" className="navbar">
    <Navbar.Brand href="/">
      <img
        src={logo}
        width="30"
        height="30"
        className="d-inline-block align-top"
        alt="Cooking App logo"
      />
      Cooking App
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-light">Search</Button>
      </Form>
      <Nav className="mr-auto menu">
        {isAuthenticated ? (
          <NavDropdown title="Menu" id="basic-nav-dropdown" alignRight>
            <NavDropdown.Item href="/">Acceuil</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/dashboard">Mon compte</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/profile">Mon profile</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/listeDeCourse">
              Ma liste de course
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/" onClick={() => logout()}>
              Se déconnecter
            </NavDropdown.Item>
          </NavDropdown>
        ) : (
          <NavDropdown title="Menu" id="basic-nav-dropdown">
            <NavDropdown.Item href="/">Acceuil</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/signup">S'inscrire</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/login">Se connecter</NavDropdown.Item>
          </NavDropdown>
        )}
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

TopNavigation.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token,
    user: state.user,
  };
}
export default connect(mapStateToProps, { logout: actions.logout })(
  TopNavigation
);
